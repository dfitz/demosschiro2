<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'demosschiro');

/** MySQL database username */
define('DB_USER', 'backd0ct0r');

/** MySQL database password */
define('DB_PASSWORD', 'backach3!');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'G;].jh[/VUT+GjXsV+g``(-berXFgyX%N6>1Mh#d>|aRo$g0cEHQ5(C(Z>y,+9fY');
define('SECURE_AUTH_KEY',  '5s+wSFoyb)d rUP~WBvtT(a=Xr5~+|Z@}w8-e>?f$- a$r)soW-tTHF>7^2+w(qD');
define('LOGGED_IN_KEY',    'jMpdl-JH+tuzX>]SAiZ3YT_I|O^NtOtm[2/B5K|A^qw0ayoQHc[Tgi:mY9`Bt7kf');
define('NONCE_KEY',        ']#} ^?`Qx!woj2@;7fc~>|]FMU+YNC!<zZQpg[v+RMBq G}C C>Z=(`_sj{1VH97');
define('AUTH_SALT',        'ZQPh=xu](wPhyF-~#zQJ~Q,s|soy2qUo&EzAZE%Gj<wIaHWg~0X&uJOv8~+mhf#.');
define('SECURE_AUTH_SALT', 'mV&]3++2/)kzH*vDPp(mtG/-wv=XT@;lsA?qK`wGcQZpapwp#?k(8A[B4QfJ^R8K');
define('LOGGED_IN_SALT',   '{&^DyEQ)U&k_@pn<uo%*PC + 0uw=+#+;u||XR]xfEFE$Btu8,I7s4il8+5CF7}0');
define('NONCE_SALT',       'En3Q^F_Eec-CA2]vA_u$|OJ6+AKvq)~Ee)yrE9]ft/}^*3WA~,)Pn.T3-&,nfC~N');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
